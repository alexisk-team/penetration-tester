import path from "path";
import fs from "fs";

const config = {
  logsUpdateIntervalMs: 1000
}

async function readLogs() {
  const pidPath = path.resolve(process.cwd(), 'pid.log');
  const summaryPath = path.resolve(process.cwd(), 'logger-summary.log');
  const filePath = path.resolve(process.cwd(), 'logger-verbose.log');
  if ( !fs.existsSync(filePath) ) {
    process.stdout.write(`File ${filePath} does not exist! Exiting...\n`);
    process.exit(1);
  }

  function printFile() {
    process.stdout.write('\u001B[2J\u001B[0;0f');
    if ( fs.existsSync(pidPath) ) {
      process.stdout.write(`Last run main process PID: ${fs.readFileSync(pidPath, {encoding: 'utf-8'})}:\n`);
    }
    if ( fs.existsSync(summaryPath) ) {
      process.stdout.write('\nSUMMARY\n');
      process.stdout.write(`Content of ${summaryPath}:\n`);
      process.stdout.write(fs.readFileSync(summaryPath, {encoding: 'utf-8'}));
      process.stdout.write('\n');
    }
    if ( fs.existsSync(filePath) ) {
      process.stdout.write('\nSESSION\n');
      process.stdout.write(`Content of ${filePath}:\n`);
      process.stdout.write(fs.readFileSync(filePath, {encoding: 'utf-8'}));
    } else {
      process.stdout.write(`Waiting for session file ${filePath} ...`);
    }

    setTimeout(printFile, config.logsUpdateIntervalMs);
  }

  setTimeout(printFile, config.logsUpdateIntervalMs);
}

readLogs();