import {SessionStatisticsCounter} from "./session-statistics-counter";
import {config} from "../config";
import {logger} from "../utils";

export interface IProcessManagerOptions {
  timeoutMs?: number
}

export class ProcessManager {
  private loggerInterval?: any;
  private runInterval?: any;
  public isRunning: boolean = false;
  public readonly stats = new SessionStatisticsCounter();
  public target: string;

  constructor(private options: IProcessManagerOptions) {}

  setTarget(target: string) {
    this.target = target;
    this.stats.setTarget(target);
    return this;
  }

  start() {
    this.stop();
    this.stats.reset();

    if (!this.loggerInterval) {
      this.loggerInterval = setInterval(() => this.stats.log(), config.misc.loggingIntervalMs);
    }
    if (this.options.timeoutMs) {
      this.runInterval = setTimeout(() => this.stop(), this.options.timeoutMs);
    }
    this.isRunning = true;

    return this;
  }

  stop() {
    if (this.runInterval) {
      clearTimeout(this.runInterval);
      this.runInterval = null;
    }
    this.isRunning = false;

    return this;
  }

  finalizeLog() {
    if (this.loggerInterval) {
      clearInterval(this.loggerInterval);
      this.loggerInterval = null;
      this.stats.log();
      logger.removeMark();
    }
    return this;
  }
}