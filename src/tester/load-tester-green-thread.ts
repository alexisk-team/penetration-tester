import {httpReqWithProxy, IProxy} from "../proxy";
import {axiosRespSuccess, generateRandomWord} from "../utils";
import {ProcessManager} from "./process-manager";

export async function loadTesterGreenThread(
    target: string,
    proxy: IProxy,
    processManager: ProcessManager
) {
  while(1) {
    if ( !processManager.isRunning ) { return; }

    const url = new URL(target);
    url.searchParams.set(generateRandomWord(), generateRandomWord());
    const resp = await httpReqWithProxy(url.href, proxy);
    if ( axiosRespSuccess(resp) ) {
      processManager.stats.success(resp.status);
    } else {
      if ( processManager.stats.fail(resp?.status) ) {
        processManager.stop();
      }
    }
  }
}