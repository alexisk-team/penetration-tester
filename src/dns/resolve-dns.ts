import * as dns from "dns";

export async function resolveDns(origin: string) {
  if (origin.indexOf('://') >= 0) {
    origin = new URL(origin).origin;
  }

  return await dns.promises.resolve4(origin);
}