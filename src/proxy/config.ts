export * from '../config';
export const locationTestUrl = "https://api.myip.com/";
export const proxyEndpoints = {
  proxyList: region => `https://www.proxy-list.download/api/v1/get?type=http&country=${region}`,
  proxyScan: region => `https://www.proxyscan.io/api/proxy?country=${region.toLowerCase()}&limit=100&level=anonymous`,
  proxyScrape: (region, protocol) => `https://api.proxyscrape.com/v2/?request=displayproxies&timeout=10000&country=${
    region.toLowerCase()}&ssl=all&anonymity=all&protocol=${protocol}`
  // Http parse
  // https://spys.one/free-proxy-list/RU/
  // http://free-proxy.cz/en/proxylist/country/RU/all/ping/all
};
export const openVPNEndpoint = "https://www.vpngate.net/api/iphone/";
