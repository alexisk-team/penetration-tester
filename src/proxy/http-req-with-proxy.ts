import {IProxy} from "./types";
import axios, {AxiosInstance, AxiosResponse} from "axios";
import {SocksProxyAgent} from "socks-proxy-agent";
import {httpReqAxios} from "../utils";

export async function httpReqWithProxy(
    target: string, proxy: IProxy): Promise<AxiosResponse> {
  let client: AxiosInstance = axios;
  let proxyOptions
  if (proxy.types.SOCKS5 || proxy.types.SOCKS4) {
    const url = `socks${proxy.types.SOCKS5 ? '5' : '4'}://${proxy.ip}:${proxy.port}`;
    client = axios.create({httpsAgent: new SocksProxyAgent(url)});
  } else {
    proxyOptions = {
      host: proxy.ip,
      port: proxy.port
    };
  }

  return await httpReqAxios(target, {client, requestOptions: {proxy: proxyOptions}});
}
