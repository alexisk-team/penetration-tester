import {IProxy, IResolveTest} from "./types";
import {config, locationTestUrl} from "./config";
import {logger, timeoutLimit} from "../utils";
import {httpReqWithProxy} from "./http-req-with-proxy";

function formatProxy(proxy: IProxy) {
  return {
    ip: proxy.ip,
    port: proxy.port,
  }
}

export async function validateProxy(proxy: IProxy, region?: string, testAddress?: IResolveTest): Promise<IResolveTest | false> {
  let response: IResolveTest | false = false;

  if ( !await timeoutLimit(async (isTimedOut) => {
    try {
      const resp = await httpReqWithProxy(locationTestUrl, proxy);
      if (isTimedOut()) {
        return;
      }
      const resolveTest: IResolveTest = resp.data;

      if (!resolveTest || !resolveTest.ip) {
        logger.error(`Fatal: Unexpected response from ip check service`);
        return false;
      }
      if (region && resolveTest.cc !== region) {
        logger.verbose.warning(`Proxy region does not match`, resolveTest);
        return false;
      }
      if (testAddress && testAddress.ip === resolveTest.ip) {
        logger.verbose.warning(`Proxy does not mask ip`, resolveTest);
        return false;
      }

      logger.verbose.success(`Proxy verified:`, resolveTest);
      return response = resolveTest;
    } catch(err) {
      if ( !isTimedOut() ) {
        logger.verbose.warning(`Proxy check error`, formatProxy(proxy));
        return false;
      }
    }
  }, config.proxy.timeoutMs) ) {
    logger.verbose.warning(`Proxy check timeout`, formatProxy(proxy));
    return false;
  }
  return response;
}