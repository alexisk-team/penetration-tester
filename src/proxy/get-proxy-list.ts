import {config} from "../config";
import {IProxy, IProxyMapping} from "./types";
import {httpReqAxios, logger} from "../utils";
import {proxyEndpoints} from "./config";

export async function getProxyList(): Promise<IProxyMapping> {
  let totalProxiesFound = 0;
  const result: IProxyMapping = {};

  await Promise.all(config.proxy.regions.map(async region => {
    const proxiesPerThisRegion = await getProxyListPerRegion(region);
    totalProxiesFound += proxiesPerThisRegion.length;
    result[region] = proxiesPerThisRegion;
  }));

  logger.verbose.info(`Fetched ${totalProxiesFound} proxies for targeted regions: [${config.proxy.regions}]`);
  return result;
}

async function getProxyListPerRegion(region): Promise<IProxy[]> {
  const differentSourcesResults = (await Promise.all([
    await getProxyPerRegionProxyList(region),
    await getProxyPerRegionProxyScan(region),
    await getProxyPerRegionProxyScrape(region),
  ]))
  return differentSourcesResults.reduce((acc, list) => [...acc, ...list], []);
}

async function getProxyPerRegionProxyList(region): Promise<IProxy[]> {
  const resp = await httpReqAxios(proxyEndpoints.proxyList(region));
  return resp.data.split('\n').map(v => v.trim()).filter(v => v.length).map(ipString => {
    const [ip, port] = ipString.split(':');
    return {
      ip,
      port: +port,
      types: {
        HTTP: true,
        HTTPS: true,
      }
    }
  });
}

async function getProxyPerRegionProxyScan(region): Promise<IProxy[]> {
  const resp = await httpReqAxios(proxyEndpoints.proxyScan(region));
  return resp.data.map(proxyData => ({
    ip: proxyData.Ip,
    port: proxyData.Port,
    types: proxyData.Type.reduce((acc, key) => {
      acc[key] = true;
      return acc;
    }, {})
  }));
}

async function getProxyPerRegionProxyScrape(region): Promise<IProxy[]> {
  return [
      ...(await _getProxyPerRegionProxyScrape(region, 'HTTPS')),
      ...(await _getProxyPerRegionProxyScrape(region, 'SOCKS4')),
      ...(await _getProxyPerRegionProxyScrape(region, 'SOCKS5')),
  ];
}

async function _getProxyPerRegionProxyScrape(region, protocol): Promise<IProxy[]> {
  const resp = await httpReqAxios(proxyEndpoints.proxyScrape(region, protocol.toLowerCase()));
  return resp.data.split('\n')
      .map(v => v.trim())
      .filter(v => v)
      .map(v => {
        const [ip, port] = v.split(':');
        return {
          ip,
          port,
          types: {
            [protocol]: true
          }
        }
      });
}
