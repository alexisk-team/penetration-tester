import axios, {AxiosResponse} from "axios";
import {logger} from "../utils";
import {openVPNEndpoint} from "./config";

export type IOpenVPNConfig = string;

export interface IOpenVPNConfigMapping {
  [countryCode: string]: Set<IOpenVPNConfig>;
}

export async function getOpenVPNConfigs(): Promise<IOpenVPNConfigMapping> {
  logger.verbose.info(`Getting vpn list from ${openVPNEndpoint}`);
  const resp = await axios.get(openVPNEndpoint);
  return parseResponse(resp.data);
}

function parseResponse(data: AxiosResponse<any, any>['data']): IOpenVPNConfigMapping {
  const rows = data.split('\n');
  rows.shift();
  const cols = rows.shift().trim().split(',');
  const countryCodeIndex = cols.indexOf('CountryShort');
  const vpnConfigIndex = cols.indexOf('OpenVPN_ConfigData_Base64');

  const result: IOpenVPNConfigMapping = {};

  rows.forEach(row => {
    const data = row.trim().split(',');
    const countryCode = data[countryCodeIndex];
    if (countryCode) {
      if (!result[countryCode]) {
        result[countryCode] = new Set();
      }
      const decodedVPNConfig = Buffer.from(data[vpnConfigIndex], 'base64').toString();
      const noCommentsVPNConfig = decodedVPNConfig
          .split('\r\n').filter(line => (line[0] !== '#') && line.trim().length)
          .join('\r\n');

      result[countryCode].add(noCommentsVPNConfig);
    }
  });
  return result;
}