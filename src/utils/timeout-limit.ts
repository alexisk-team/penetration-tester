export async function timeoutLimit(worker: (isTimedOut: () => boolean) => any, timeMs): Promise<boolean> {
  return new Promise(async resolve => {
    let isTimedOut = false;
    const timeout = setTimeout(() => {
      isTimedOut = true;
      resolve(false);
    }, timeMs);
    await worker(() => isTimedOut);
    clearTimeout(timeout);
    resolve(true);
  });
}
