import {formatTimeDiff} from "./format-date";

test('formatTimeDiff', async () => {
  expect(formatTimeDiff(0)).toEqual('0sec 0ms');
  expect(formatTimeDiff(10)).toEqual('0sec 10ms');
  expect(formatTimeDiff(1020)).toEqual('1sec 20ms');
  expect(formatTimeDiff(102030)).toEqual('1min 42sec');
  expect(formatTimeDiff(10203040)).toEqual('2h 50min');
  expect(formatTimeDiff(1020304050)).toEqual('11day(s) 19h');
  expect(formatTimeDiff(102030405060)).toEqual('1180day(s) 21h');
})