export function countSubstrings(str: string, subStr: string, allowOverlapping = false): number {

  str += "";
  subStr += "";
  if (subStr.length <= 0) return (str.length + 1);

  let n = 0,
      pos = 0,
      step = allowOverlapping ? 1 : subStr.length;

  while (true) {
    pos = str.indexOf(subStr, pos);
    if (pos >= 0) {
      ++n;
      pos += step;
    } else break;
  }
  return n;
}
