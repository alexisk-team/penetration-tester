import * as http from "http";

const defaultOptions = {
  headers: {
    'content-type': 'application/json',
    'accept': '*/*',
    'connection': 'keep-alive',
  }
}

const optionsPresets = {
  get: {
    method: 'GET',
    port: '80',
    ...defaultOptions
  },
  post: {
    method: 'POST',
    port: '443',
    ...defaultOptions
  },
}

export async function httpReqRaw(method: string, path: string, payload?: any) {
  const url = new URL(path);
  const options = {
    ...optionsPresets[method.toLowerCase()],
    hostname: url.hostname,
    path: url.pathname + url.search
  }
  return new Promise((resolve) => {
    http.request(options, res => {
      resolve(res);
    })
  });
}