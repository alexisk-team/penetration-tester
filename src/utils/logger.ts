import {formatDate} from "./format-date";
import path from "path";
import {OutputBuffer} from "./output-buffer";

const color = {
  red: '\x1b[31m',
  green: '\x1b[32m',
  yellow: '\x1b[33m',
  blue: '\x1b[33m',
  magenta: '\x1b[33m',
  cyan: '\x1b[33m',
}

const format = {
  reset: '\x1b[0m',
  bright: '\x1b[1m',
  dim: '\x1b[2m',
  underscore: '\x1b[4m',
  blink: '\x1b[5m',
}

const timeColumnEmptyString = new Array(formatDate(new Date()).length).fill(' ').join('');

export type ILogger = Logger & {
  verbose: Logger,
  debug: Logger
};

export class Logger {
  private lastDate: string;
  private outputBuffer: OutputBuffer;
  private childLoggers: Logger[] = [];

  constructor(public name: string, inheritFrom?: Logger) {
    this.outputBuffer = new OutputBuffer(path.join(process.cwd(), `logger-${this.name}.log`));
    if (inheritFrom) {
      inheritFrom.addChild(this);
    }
  }

  private static formatStrings(value: any) {
    if (typeof value === "string") {
      return value.replace(/\n/g, `\n${timeColumnEmptyString}   `);
    }
    return value;
  }

  private addChild(logger: Logger) {
    this.childLoggers.push(logger);
  }

  private triggerChildren<T extends keyof Logger>(method: T, args?: any) {
    this.childLoggers.forEach(loggerRef => {
      //@ts-expect-error
      loggerRef[method](...args);
    });
  }

  mark(key?: string) {
    this.outputBuffer.mark(key);
    this.triggerChildren('mark', [key]);
    return this;
  }

  removeMark(key?: string) {
    this.outputBuffer.removeMark(key);
    this.triggerChildren('removeMark', [key]);
    return this;
  }

  clean(key?: string) {
    this.outputBuffer.clean(key);
    this.triggerChildren('clean', [key]);
    return this;
  }

  line() {
    this.outputBuffer.write('');
    return this;
  }

  _log(data: any[], c?: keyof typeof color, separator = ' ') {
    let dateString = formatDate(new Date());
    if (dateString === this.lastDate) {
      dateString = timeColumnEmptyString;
    } else {
      this.lastDate = dateString;
    }

    const strings = [`${
        format.dim}${
        dateString}${
        format.reset} ${
        c ? color[c] : ''}${
        separator}${
        format.reset}`, ...data.map(v => {
      return Logger.formatStrings(OutputBuffer.anyValueToString(v));
    })];

    this._logWrite(strings);
    this.triggerChildren('_logWrite', [strings]);

    return this;
  }

  _logWrite(data: string[]) {
    this.outputBuffer.write(...data);
  }

  info(...data) {
    return this._log(data);
  }

  important(...data) {
    return this._log(data, 'blue', '*');
  }

  success(...data) {
    return this._log(data, 'green', '✓');
  }

  warning(...data) {
    return this._log(data, 'yellow', '!');
  }

  error(...data) {
    return this._log(data, 'red', 'E');
  }
}

export const summaryLogger = new Logger('summary');
export const logger: ILogger = new Logger('simple') as ILogger;
logger.verbose = new Logger('verbose', logger);
