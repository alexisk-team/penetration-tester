const baseConfig = require('../config.json');

let configOverride;
try {
  configOverride = require('../config-override.json');
} catch (err) {
  configOverride = {};
}

export const config: typeof baseConfig = {...baseConfig, ...configOverride};
